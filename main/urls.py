from django.urls import path

from . import views
from .views import add, delete

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('<int:id>', views.matkul, name='matkul'),
    path('add/', views.add, name='add'),
    path('delete/<int:id>', delete, name = 'delete'),
]
