# Generated by Django 3.1.2 on 2020-10-17 03:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.TextField(max_length=50)),
                ('dosen', models.TextField(max_length=50)),
                ('sks', models.TextField()),
                ('deskripsi', models.TextField(max_length=50)),
                ('semester', models.TextField(max_length=50)),
            ],
        ),
    ]
