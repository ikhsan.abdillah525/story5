from django.shortcuts import render, redirect
from .models import Matkul
from .forms import FormJadwal

def home(request):
    allMatkul = Matkul.objects.all()
    
    return render(request, 'main/home.html', {"allMatkul": allMatkul})

def matkul(request, id):
    matkul = Matkul.objects.get(id = id)
    
    return render(request, 'main/matkul.html', {"matkul": matkul})

def add(request):
    if request.method == "POST":
        form = FormJadwal(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            dosen = form.cleaned_data['dosen']
            sks = form.cleaned_data['sks']
            deskripsi = form.cleaned_data['deskripsi']
            semester = form.cleaned_data['semester']
            ruang = form.cleaned_data['ruang']
            item = Matkul(nama=nama, dosen=dosen, sks=sks, deskripsi=deskripsi, semester=semester, ruang=ruang)
            item.save()

            return redirect('../')
    else: 
        form = FormJadwal()
    
    return render(request, 'main/add.html', {"form" : form})

def delete(request, id):
    if request.method == "GET":
        matkul = Matkul.objects.get(id = id)
        matkul.delete()
        
        return redirect('../')
    
    return redirect('../')