from django.db import models

# Create your models here.
class Matkul(models.Model):
    nama = models.TextField(max_length=50)
    dosen = models.TextField(max_length=50)
    sks = models.TextField()
    deskripsi = models.TextField(max_length=50)
    semester = models.TextField(max_length=50)
    ruang = models.TextField(max_length=50)