from django import forms

class FormJadwal(forms.Form):
    nama = forms.CharField(label="Nama matkul")

    dosen = forms.CharField(label="Dosen")

    sks = forms.CharField(label="Jumlah SKS")

    deskripsi = forms.CharField(label="Deskripsi")

    semester = forms.CharField(label="Semester")

    ruang = forms.CharField(label="Ruang")